from socket import *
serverName = 'LAPTOP-7CJR19VH'
serverPort = 12000
clientSocket = socket(AF_INET, SOCK_STREAM)
clientSocket.connect((serverName, serverPort))
sentence = ('Input lowercase sentence:')
sentenceAsByte = str.encode(sentence)
clientSocket.send(sentenceAsByte)
modifiedSentence = clientSocket.recv(1024)
print ("From Server:", modifiedSentence)
clientSocket.close()